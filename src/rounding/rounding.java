package rounding;

import java.util.Scanner;

public class rounding {

    public static void main(String[] args) {
        double number = 0.0;
        double numberInt = 0;

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number!");
        number = sc.nextDouble();
        sc.close();

        numberInt = Math.round(number);
        
        if (numberInt % 3 == 0) {
            System.out.println("Number is divisible through 3.");
        } else {
            System.out.println("Number is NOT divisible through 3.");
        }
    }
}
