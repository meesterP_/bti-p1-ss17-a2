package seconds;

import java.util.Scanner;

public class SecondsConverter {

    public static void main(String[] args) {
        int days = 0;
        int hours = 0;
        int minutes = 0;
        int seconds = 0;
        int dayInSeconds = 86400;
        int hourInSeconds = 3600;
        int minuteInSeconds = 60;

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter seconds you want to convert!");
        seconds = sc.nextInt();
        sc.close();

        if (seconds < 0) {
            System.out.println("Illegal paramter!");
        } else {
            days = seconds / dayInSeconds;
            seconds -= days * dayInSeconds;

            hours = seconds / hourInSeconds;
            seconds -= hours * hourInSeconds;

            minutes = seconds / minuteInSeconds;
            seconds -= minutes * minuteInSeconds;

            System.out.println(days + " Days " + hours + ":" + minutes + ":" + seconds);
        }
    }
}
